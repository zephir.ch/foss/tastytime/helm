# How To

## Prerequisites

+ install: https://k3s.io/
+ install: https://helm.sh/

+ k3s kubectl -n kube-system create serviceaccount tiller
serviceaccount/tiller created
+ k3s kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
clusterrolebinding.rbac.authorization.k8s.io/tiller created


## Test Chart

+ install: helm install `<XYZ>` `<CHART_FOLDER>` (example `helm install foobar ./mychart`)
+ test: helm get manifest `<XYZ>`
+ uninstall: helm uninstall `<XYZ>`
+ test as dry run: `helm install --debug --dry-run <XYZ> <CHART_FOLDER>`